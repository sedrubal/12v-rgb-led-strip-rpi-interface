# 12V RGB LED Strip Driver for RPi

## BOM:

- 3 x MOSFET (`L2203N`)
- 3 x 1 kOhm (between RPi & MOSFET Gate) (maybe 10 kOhm are ok, too)
- 3 x 100 kOhm (between MOSFET Gate & GND) (maybe also 10 kOhm are ok, too)
- 1 x 12V DC Power Supply
- 1 x Hohlbuchse
- 1 x 4 PIN Header (for LED Strip - RGB & GND)
- 1 x 4 PIN Header (for RPi GPIO - RGB & GND)
